﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Keyence.AutoID;

namespace BarcodeReader2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private Keyence.AutoID.BarcodeReaderControl barcodeReaderControl1;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.barcodeReaderControl1 = new Keyence.AutoID.BarcodeReaderControl();
            this.barcodeReaderControl1.BackColor = System.Drawing.Color.Lime;
            this.barcodeReaderControl1.IpAddress = "0.0.0.0";
            this.barcodeReaderControl1.LiveView.ImageType = Keyence.AutoID.ImageType.JEPG_IMAGE;
            this.barcodeReaderControl1.LiveView.WindowScale = 2;
            this.barcodeReaderControl1.Location = new System.Drawing.Point(12, 12);
            this.barcodeReaderControl1.Name = "barcodeReaderControl1";
            this.barcodeReaderControl1.Size = new System.Drawing.Size(400, 300);
            this.barcodeReaderControl1.TabIndex = 29;

            this.windowsFormsHost1.Child = this.barcodeReaderControl1;

            //
            // Register handler for reading data received event
            //
            this.barcodeReaderControl1.OnDataReceived += new Keyence.AutoID.BarcodeReaderControl.OnDataReceivedEventHandler(barcodeReaderControl1_OnDataReceived);
            //
            // Set the type of reader to connect
            //
            this.barcodeReaderControl1.ReaderType = ReaderType.SR_700;
            //
            // Set the interface to connect the reader
            //
            this.barcodeReaderControl1.Comm.Interface = Interface.USB;
        }

        private void barcodeReaderControl1_OnDataReceived(object sender, OnDataReceivedEventArgs e)
        {
            //
            // Delegate display processing of the received data to the textBox
            //
            this.textBox2.Dispatcher.Invoke(new updateTextBoxDelegate(updateTextBox), e.data);
        }

        //
        // Delegated function of the textBox
        //
        private delegate void updateTextBoxDelegate(byte[] data);
        private void updateTextBox(byte[] data)
        {
            //
            // Display the received data to the textBox 
            //
            this.textBox2.Text = Encoding.GetEncoding("Shift_JIS").GetString(data);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //
                // Connect to the Reader
                //
                this.barcodeReaderControl1.Connect();
                this.textBox1.Text = "Connected successfully";
                if (this.barcodeReaderControl1.Comm.Interface == Interface.USB)
                {
                    //
                    // Send "SKCLOSE" in order to occupy the data port connection
                    //
                    this.barcodeReaderControl1.SKCLOSE();
                }
                else
                {
                    //
                    // Make sure that command response character string is specified.
                    //
                    string val = this.barcodeReaderControl1.RP("610");
                    switch (this.barcodeReaderControl1.ReaderType)
                    {
                        case ReaderType.SR_1000:
                        case ReaderType.SR_700:
                            if (!val.Equals("1"))
                            {
                                this.textBox1.Text = "Set Baseic command response string to Detailed response.";
                            }
                            break;
                        case ReaderType.SR_D100:
                        case ReaderType.SR_750:
                            if (!val.Equals("0"))
                            {
                                this.textBox1.Text = "Disable the setting of Specify response character.";
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                this.textBox1.Text = ex.Message;
            }
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            this.textBox1.Text = "";
            try
            {
                //
                // Start processing of LiveView
                //
                this.barcodeReaderControl1.StartLiveView();
            }
            catch (Exception ex)
            {
                this.textBox1.Text = ex.Message;
            }
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            this.textBox1.Text = "";
            try
            {
                //
                // Send LON command
                //
                this.barcodeReaderControl1.LON();
            }
            catch (CommandException cex)
            {
                //
                // ExtErrCode shows the number of command error
                //
                this.textBox1.Text = "Command err," + cex.ExtErrCode;
            }
            catch (Exception ex)
            {
                this.textBox1.Text = ex.Message;
            }
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            this.textBox1.Text = "";
            try
            {
                //
                // Send LOFF command
                //
                this.barcodeReaderControl1.LOFF();
            }
            catch (Exception ex)
            {
                this.textBox1.Text = ex.Message;
            }
        }
    }
}
